open Crowbar
open Fuzz_data_encoding_ty

let tytup_gen (ty_gen : any_ty gen) : any_tytup gen =
  choose [
    map
      [ty_gen]
      (fun (Any t1) ->
        Any_tup (T1(t1)));
    map
      [ty_gen; ty_gen]
      (fun (Any t1) (Any t2) ->
        Any_tup (T2(t1,t2)));
    map
      [ty_gen; ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3)  ->
        Any_tup (T3(t1,t2,t3)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) ->
        Any_tup (T4(t1,t2,t3,t4)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) (Any t5) ->
        Any_tup (T5(t1,t2,t3,t4,t5)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen; ty_gen;
       ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) (Any t5)
           (Any t6) ->
        Any_tup (T6(t1,t2,t3,t4,t5,t6)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen; ty_gen;
       ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) (Any t5)
           (Any t6) (Any t7) ->
        Any_tup (T7(t1,t2,t3,t4,t5,t6,t7)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen; ty_gen;
       ty_gen; ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) (Any t5)
           (Any t6) (Any t7) (Any t8) ->
        Any_tup (T8(t1,t2,t3,t4,t5,t6,t7,t8)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen; ty_gen;
       ty_gen; ty_gen; ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) (Any t5)
           (Any t6) (Any t7) (Any t8) (Any t9) ->
        Any_tup (T9(t1,t2,t3,t4,t5,t6,t7,t8,t9)));
    map
      [ty_gen; ty_gen; ty_gen; ty_gen; ty_gen;
       ty_gen; ty_gen; ty_gen; ty_gen; ty_gen]
      (fun (Any t1) (Any t2) (Any t3) (Any t4) (Any t5)
           (Any t6) (Any t7) (Any t8) (Any t9) (Any t10) ->
        Any_tup (T10(t1,t2,t3,t4,t5,t6,t7,t8,t9,t10)));
  ]
