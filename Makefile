BINDIR := _build/default
FUZZ := fuzz_data_encoding.exe
RESULTS := test_results.exe

all:
	@echo "Choose a target about 'test' (random testing), 'fuzz' (afl-fuzz) or 'results'"

build: $(BINDIR)/$(FUZZ)

.PHONY: $(BINDIR)/$(FUZZ)
$(BINDIR)/$(FUZZ):
	jbuilder build $(FUZZ)

.PHONY: clean
clean:
	jbuilder clean

test: $(BINDIR)/$(FUZZ)
	$(BINDIR)/$(FUZZ)

fuzz: $(BINDIR)/$(FUZZ)
	@mkdir -p input
	@echo foobar > input/testcase
	@if [[ $$(cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor) == "powersave" ]]; then \
	  export AFL_SKIP_CPUFREQ=1; \
	fi; \
	afl-fuzz -i input -o output $(BINDIR)/$(FUZZ) @@

results: $(BINDIR)/$(RESULTS)
	$(BINDIR)/$(RESULTS)

.PHONY: $(BINDIR)/$(RESULTS)
$(BINDIR)/$(RESULTS):
	jbuilder build $(RESULTS)
