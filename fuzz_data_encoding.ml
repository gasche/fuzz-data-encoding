open Crowbar
open Fuzz_data_encoding_ty
open Fuzz_data_encoding_gen
open Fuzz_data_encoding_ops

let value_gen : value gen =
  dynamic_bind ty_gen @@ fun (Any ty) ->
  let op = ops ty in
  map [op.gen] (fun v -> Value (op, v))

type 'data encoder = {
  encode : 'a . 'a D.encoding -> 'a -> 'data;
  decode : 'a . 'a D.encoding -> 'data -> 'a;
}

let check encoder (Value (op, v)) =
  let data = encoder.encode op.encoding v in
  match encoder.decode op.encoding data with
  | exception exn ->
     Printf.ksprintf fail
       "Incorrect deserialization (%s)" (Printexc.to_string exn)
  | v' -> check_eq ~pp:op.printer ~eq:op.eq v v'

let json = { encode = D.Json.construct; decode = D.Json.destruct }
let bson = { encode = D.Bson.construct; decode = D.Bson.destruct }
let binary = { encode = D.Binary.to_bytes; decode = D.Binary.of_bytes_exn }

let () =
  add_test ~name:"JSON encoding" [value_gen] (check json);
  add_test ~name:"BSON encoding" [value_gen] (check bson);
  add_test ~name:"Binary encoding" [value_gen] (check binary);
